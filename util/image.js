const fs = require('fs');
const fetch = require('node-fetch');
var glob = require("glob");

exports.putImg = async (url, name) => {
    try {
        const response = await fetch(url);  // 이미지 요청
        let type = response.headers.get('content-type').split('/')[1] // 확장자타입 추출
        type = type.split('+')[0];
        const imgName = name+type;   // 저장할 이미지 이름
    
        const buffer = await response.buffer();
        fs.writeFile('images/'+imgName, buffer, () => console.log('finished downloading image'));   // 이미지 저장

        return imgName;
    } catch (err) {
        console.log(err); 
        return false;
    }
}

exports.delPrev = async (idx) => {
    try {   
        glob('images/prev_'+idx+'*', function (er, files) {
            for (const file of files) {
                fs.unlinkSync(file)
            }
        })
    } catch (err) {
        console.log(err); 
        return false;
    }
}

exports.delMark = async (idx) => {
    try {   
        glob('images/mark_'+idx+'_*', function (er, files) {
            for (const file of files) {
                fs.unlinkSync(file)
            }
        })
    } catch (err) {
        console.log(err); 
        return false;
    }
}