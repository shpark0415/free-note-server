exports.parseUrl = (url) => {
    let temp = url.split('/').reverse()[0];
    console.log(temp.split(/(.png|.jpg|.jpeg|.gif)/ig))
    return temp.split(/(.png|.JPG|.jpeg|.gif)$/ig)
}

exports.getFilePath = (item) => {
    const env = require('../conf/env');
    if(item && item.filePath) item.filePath = env.BASEURL+item.filePath;
    return item;
}