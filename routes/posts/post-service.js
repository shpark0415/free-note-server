const pool = require('../../conf/db_info');	  
const metadata = require('../../crawling/metadata');
const map = require('../../crawling/map');
const url = require('../../util/url');
const image = require('../../util/image');
const request = require('request-promise');
const env = require('../../conf/env');

exports.categoryList = async (user_id) => {
    const sql = 'SELECT idx, name FROM CATEGORY where user_id=?'

    try {
        let res = await pool.query(sql, [user_id])
        return res[0];
    } catch(err) {
        throw Error(err)
    }
}

exports.addCategory = async (user_id, params) => {
    let conn = await pool.getConnection()

    const sql1 = 'insert into CATEGORY set user_id=?, name=?';
    const sql2 = 'select idx from CATEGORY where user_id=? and name=?';

    try {
        await conn.beginTransaction()

        let res1 = await pool.query(sql1, [user_id, params.name])
        if(res1[0].affectedRows == 1) {
            // api추가
            let res2 = await pool.query(sql2, [user_id, params.name])
            await conn.commit()
            return {result: true, message: 'success', idx: res2[0][0].idx}
        }
        else return {result: false, message: 'fail'}
    } catch(err) {
        conn.rollback();
        throw Error(err)
    } finally {
        conn.release()
    }
}

exports.modCategory = async (user_id, params) => {
    const sql = 'update CATEGORY set name=? where user_id=? and name=?';
    
    try {
        let res = await pool.query(sql, [params.next, user_id, params.prev])
        if(res[0].affectedRows == 1) return {result: true, message: 'success'}
        else return {result: false, message: 'fail'}
    } catch(err) {
        throw Error(err)
    }
}

exports.delCategory = async (params) => {
    const sql = 'delete from CATEGORY where idx=?';
    
    try {
        let res = await pool.query(sql, [params.idx])
        if(res[0].affectedRows == 1) return {result: true, message: 'success'}
        else return {result: false, message: 'fail'}
    } catch(err) {
        throw Error(err)
    }
}

exports.getMapInfo = async (params) => {
    let result = []
    try {
        // map crawling
        result = await map.chkUrlType(params.url);
        return result;
    } catch(err) {
        throw Error(err)
    }
}

exports.addPost = async (user_id, p) => {
    let sql = null;
    let res = null;
    let prevSql = null;
    let prevRes = null;
    let markFlag = false;

    try {
        if(p.useExpire){
            sql = 'insert into POST set user_id=?, category_idx=?, title=?, url=?, content=?, star=?, `lock`=?, expire=?';
            res = await pool.query(sql, [user_id, p.category_idx, p.title, p.url, p.content, p.star, p.lock, p.expire])
        } else {
            sql = 'insert into POST set user_id=?, category_idx=?, title=?, url=?, content=?, star=?, `lock`=?';
            res = await pool.query(sql, [user_id, p.category_idx, p.title, p.url, p.content, p.star, p.lock])
        }

        // post insert 성공하면 크롤링후 insert
        // post insert 성공하면 무조건 return 200 code
        if(res[0].affectedRows == 1) {
            try { 
                // 2. metadata crawling
                const metaInfo = await metadata.chkUrlType(p.url);
                
                // 이미지저장
                if(metaInfo.image) {
                    const imgName = await image.putImg(metaInfo.image, 'prev_'+res[0].insertId+'.')    // imgUrl, imgName(확장자제외)
                    prevSql = 'insert into PREVIEW set post_idx=?, title=?, content=?, filePath=?, url=?';
                    prevRes = await pool.query(prevSql, [res[0].insertId, metaInfo.title, metaInfo.description, '/img/'+imgName, metaInfo.url])
                } else {    // 이미지가 없는 경우
                    prevSql = 'insert into PREVIEW set post_idx=?, title=?, content=?, url=?';
                    prevRes = await pool.query(prevSql, [res[0].insertId, metaInfo.title, metaInfo.description, metaInfo.url])
                }
                
                // 마크가 있으면
                if(p.mark.length > 0) { 
                    // 3. 마크저장
                    let markSql = ''
                    let markRes = ''

                    await p.mark.map(async (item, n) => {
                        let metaInfo2 = await metadata.chkUrlType(item.url);    // 네이버플레이스의 URL

                        // 이미지저장
                        if(metaInfo2.image) {
                            const imgName = await image.putImg(metaInfo2.image, 'mark_'+res[0].insertId+'_'+(n+1)+'.')    // imgUrl, imgName(확장자제외)
                            markSql = 'insert into PLACE set post_idx=?, user_id=?, title=?, content=?, filePath=?, address=?, latitude=?, longitude=?, url=?';
                            markRes = await pool.query(markSql, [res[0].insertId, user_id, metaInfo2.title, metaInfo2.description, '/img/'+imgName, item.address, item.latitude, item.longitude, metaInfo2.url])
                            
                            if(markRes[0].affectedRows == 1) markFlag = true;
                            else markFlag = false;
                        } else {    // 이미지가 없는 경우
                            markSql = 'insert into PLACE set post_idx=?, user_id=?, title=?, content=?, address=?, latitude=?, longitude=?, url=?';
                            markRes = await pool.query(markSql, [res[0].insertId, user_id, metaInfo2.title, metaInfo2.description, item.address, item.latitude, item.longitude, metaInfo2.url])
                            
                            if(markRes[0].affectedRows == 1) markFlag = true;
                            else markFlag = false;
                        }
                    })
                }
                
                // if(prevRes[0].affectedRows == 1 && markFlag) return {result: true, message: 'success'};
                // else {
                //     if(markFlag === false) return {result: false, message: 'fail insert mark metadata'}
                //     else return {result: false, message: 'fail insert metadata'}
                // }

                return {result: true, message: 'success'};
            } catch(err) { 
                console.log(err); 
                return {result: false, message: err} 
            }    
        }

        else return {result: false, message: 'fail'}
    } catch(err) {
        throw Error(err)
    }
}

exports.getMarkFB = async (params) => {
    try {
        // metadata crawling
        let result = await metadata.facebookMark(params.url);
        return result;
    } catch(err) {
        throw Error(err)
    }
}

// post + mark 저장
exports.addPostMarkFB = async (user_id, p) => {
    let sql = null;
    let res = null;

    try {
        if(p.useExpire){
            sql = 'insert into POST set user_id=?, category_idx=?, title=?, url=?, content=?, star=?, `lock`=?, expire=?';
            res = await pool.query(sql, [user_id, p.category_idx, p.title, p.url, p.content, p.star, p.lock, p.expire])
        } else {
            sql = 'insert into POST set user_id=?, category_idx=?, title=?, url=?, content=?, star=?, `lock`=?';
            res = await pool.query(sql, [user_id, p.category_idx, p.title, p.url, p.content, p.star, p.lock])
        }

        // post insert 성공하면 무조건 return 200 code
        if(res[0].affectedRows == 1) {
            try { 
                // 2. metadata crawling
                const metaInfo = p.preview;                
                let prevSql = null;
                let prevRes = null;

                // 이미지저장
                if(metaInfo.image) {
                    const imgName = await image.putImg(metaInfo.image, 'prev_'+res[0].insertId+'.')    // imgUrl, imgName(확장자제외)
                    prevSql = 'insert into PREVIEW set post_idx=?, title=?, content=?, filePath=?, url=?';
                    prevRes = await pool.query(prevSql, [res[0].insertId, metaInfo.title, metaInfo.description, '/img/'+imgName, metaInfo.url])
                } else {    // 이미지가 없는 경우
                    prevSql = 'insert into PREVIEW set post_idx=?, title=?, content=?, url=?';
                    prevRes = await pool.query(prevSql, [res[0].insertId, metaInfo.title, metaInfo.description, metaInfo.url])
                }

                // 3. 마크저장
                // p.tags로 카카오 플레이스 api호출
                let tagsSql = ''
                let tagsRes = ''

                await p.tags.map(async (keyword, n) => {
                    let item = await getKakaoPlace(keyword);
                    console.log(keyword, item)
                    if(item !== null) {
                        let metaInfo2 = await metadata.chkUrlType(item.url);    // 카카오 플레이스의 URL

                        // 이미지저장
                        if(metaInfo2.image) {
                            const imgName = await image.putImg(metaInfo2.image, 'mark_'+res[0].insertId+'_'+(n+1)+'.')    // imgUrl, imgName(확장자제외)
                            tagsSql = 'insert into PLACE set post_idx=?, user_id=?, title=?, content=?, filePath=?, address=?, latitude=?, longitude=?, url=?';
                            tagsRes = await pool.query(tagsSql, [res[0].insertId, user_id, item.title, item.description, '/img/'+imgName, item.address, item.latitude, item.longitude, item.url])
                        } else {    // 이미지가 없는 경우
                            tagsSql = 'insert into PLACE set post_idx=?, user_id=?, title=?, content=?, address=?, latitude=?, longitude=?, url=?';
                            tagsRes = await pool.query(tagsSql, [res[0].insertId, user_id, item.title, item.description, item.address, item.latitude, item.longitude, item.url])
                        }
                    }                    
                })  
                
                return {result: true, message: 'success'};

            } catch(err) { 
                console.log(err); 
                return {result: false, message: err} 
            }    
        }

        else return {result: false, message: 'fail'}
    } catch(err) {
        throw Error(err)
    }
}

exports.getPost = async (user_id, params) => {
    const sql = 'SELECT * FROM POST where user_id=? and idx=?'
    const categorySql = 'select name from CATEGORY where idx=?'
    const prevSql = 'select * from PREVIEW where post_idx=?'
    const markSql = 'select * from PLACE where post_idx=? order by title'

    try {
        let res = await pool.query(sql, [user_id, params.idx])
        if(res[0].length>0) {
            // 카테고리 저장
            let res2 = await pool.query(categorySql, [res[0][0].category_idx])
            res[0][0].category_name = res2[0][0].name

            // preview 저장
            let res3 = await pool.query(prevSql, [params.idx])
            res3[0][0] = url.getFilePath(res3[0][0])   // getFullPath 
            res[0][0].preview = res3[0][0]

            // mark 저장
            let res4 = await pool.query(markSql, [params.idx])
            res4[0].map((item, idx)=>
                res4[0][idx] = url.getFilePath(item)   // getFullPath 
            )
            res[0][0].mark = res4[0]

            return res[0][0];
        }
        else return false;
    } catch(err) {
        throw Error(err)
    }
}

exports.modPost = async (user_id, p) => {
    let sql = null;
    let res = null;

    try {
        if(p.useExpire){
            sql = 'update POST set category_idx=?, title=?, url=?, content=?, star=?, `lock`=?, expire=? where idx=?';
            res = await pool.query(sql, [p.category_idx, p.title, p.url, p.content, p.star, p.lock, p.expire, p.idx])
        } else {
            sql = 'update POST set category_idx=?, title=?, url=?, content=?, star=?, `lock`=? where idx=?';
            res = await pool.query(sql, [p.category_idx, p.title, p.url, p.content, p.star, p.lock, p.idx])
        }

        // post insert 성공하면 크롤링후 insert
        // post insert 성공하면 무조건 return 200 code
        if(res[0].affectedRows == 1) {
            try { 
                if(p.urlUpdate) {   // url 업데이트     
                    if(p.url === '') {  // url이 없으면 preview, mark 모두제거후 종료
                        await removResources(p.idx);
                        return {result: true, message: 'success'};
                    }   

                    await removResources(p.idx);

                    // 2. metadata crawling
                    const metaInfo = await metadata.chkUrlType(p.url);

                    let prevSql = ''
                    let prevRes = ''
                    
                    // 이미지저장
                    if(metaInfo.image) {
                        const imgName = await image.putImg(metaInfo.image, 'prev_'+p.idx+'.')    // imgUrl, imgName(확장자제외)
                        prevSql = 'insert into PREVIEW set post_idx=?, title=?, content=?, filePath=?, url=?';
                        prevRes = await pool.query(prevSql, [p.idx, metaInfo.title, metaInfo.description, '/img/'+imgName, metaInfo.url])
                    } else {    // 이미지가 없는 경우
                        prevSql = 'insert into PREVIEW set post_idx=?, title=?, content=?, url=?';
                        prevRes = await pool.query(prevSql, [p.idx, metaInfo.title, metaInfo.description, metaInfo.url])
                    }
                    
                    // 마크가 있으면
                    if(p.mark.length > 0) { 
                        // 3. 마크저장
                        let markSql = ''
                        let markRes = ''
                        

                        await p.mark.map(async (item, n) => {
                            let metaInfo2 = await metadata.chkUrlType(item.url);    // 네이버플레이스의 URL

                            // 이미지저장
                            if(metaInfo2.image) {
                                const imgName = await image.putImg(metaInfo2.image, 'mark_'+p.idx+'_'+(n+1)+'.')    // imgUrl, imgName(확장자제외)
                                markSql = 'insert into PLACE set post_idx=?, user_id=?, title=?, content=?, filePath=?, address=?, latitude=?, longitude=?, url=?';
                                markRes = await pool.query(markSql, [p.idx, user_id, metaInfo2.title, metaInfo2.description, '/img/'+imgName, item.address, item.latitude, item.longitude, metaInfo2.url])
                            } else {    // 이미지가 없는 경우
                                markSql = 'insert into PLACE set post_idx=?, user_id=?, title=?, content=?, address=?, latitude=?, longitude=?, url=?';
                                markRes = await pool.query(markSql, [p.idx, user_id, metaInfo2.title, metaInfo2.description, item.address, item.latitude, item.longitude, metaInfo2.url])
                            }
                        })
                    }

                    return {result: true, message: 'success'};
                } 
            } catch(err) { 
                console.log(err); 
                return {result: false, message: err} 
            }            
        }

        else return {result: false, message: 'fail'}
    } catch(err) {
        throw Error(err)
    }
}

exports.modPostMarkFB = async (user_id, p) => {
    let sql = null;
    let res = null;

    try {
        if(p.useExpire){
            sql = 'update POST set category_idx=?, title=?, url=?, content=?, star=?, `lock`=?, expire=? where idx=?';
            res = await pool.query(sql, [p.category_idx, p.title, p.url, p.content, p.star, p.lock, p.expire, p.idx])
        } else {
            sql = 'update POST set category_idx=?, title=?, url=?, content=?, star=?, `lock`=? where idx=?';
            res = await pool.query(sql, [p.category_idx, p.title, p.url, p.content, p.star, p.lock, p.idx])
        }

        // post insert 성공하면 무조건 return 200 code
        if(res[0].affectedRows == 1) {
            try { 
                if(p.urlUpdate) {   // url 업데이트     
                    if(p.url === '') {  // url이 없으면 preview, mark 모두제거후 종료
                        await removResources(p.idx);
                        return {result: true, message: 'success'};
                    }   

                    await removResources(p.idx);

                    // 2. metadata crawling
                    const metaInfo = p.preview;
                    let prevSql = null;
                    let prevRes = null;
                    
                    // 이미지저장
                    if(metaInfo.image) {
                        const imgName = await image.putImg(metaInfo.image, 'prev_'+p.idx+'.')    // imgUrl, imgName(확장자제외)
                        prevSql = 'insert into PREVIEW set post_idx=?, title=?, content=?, filePath=?, url=?';
                        prevRes = await pool.query(prevSql, [p.idx, metaInfo.title, metaInfo.description, '/img/'+imgName, metaInfo.url])
                    } else {    // 이미지가 없는 경우
                        prevSql = 'insert into PREVIEW set post_idx=?, title=?, content=?, url=?';
                        prevRes = await pool.query(prevSql, [p.idx, metaInfo.title, metaInfo.description, metaInfo.url])
                    }

                    // 3. 마크저장
                    let tagsSql = null;
                    let tagsRes = null;

                    await p.tags.map(async (keyword, n) => {
                        let item = await getKakaoPlace(keyword);                        

                        if(item !== null) {
                            let metaInfo2 = await metadata.chkUrlType(item.url);    // 카카오 플레이스의 URL

                            // 이미지저장
                            if(metaInfo2.image) {
                                const imgName = await image.putImg(metaInfo2.image, 'mark_'+p.idx+'_'+(n+1)+'.')    // imgUrl, imgName(확장자제외)
                                tagsSql = 'insert into PLACE set post_idx=?, user_id=?, title=?, content=?, filePath=?, address=?, latitude=?, longitude=?, url=?';
                                tagsRes = await pool.query(tagsSql, [p.idx, user_id, item.title, item.description, '/img/'+imgName, item.address, item.latitude, item.longitude, item.url])
                            } else {    // 이미지가 없는 경우
                                tagsSql = 'insert into PLACE set post_idx=?, user_id=?, title=?, content=?, address=?, latitude=?, longitude=?, url=?';
                                tagsRes = await pool.query(tagsSql, [p.idx, user_id, item.title, item.description, item.address, item.latitude, item.longitude, item.url])
                            }
                        }                        
                    })                    
                    return {result: true, message: 'success'};
                } 
            } catch(err) { 
                console.log(err); 
                return {result: false, message: err} 
            }            
        }

        else return {result: false, message: 'fail'}
    } catch(err) {
        throw Error(err)
    }
}

exports.delPost = async (params) => {
    const sql = 'delete from POST where idx=?';
    
    try {
        await removResources(params.idx);
        let res = await pool.query(sql, [params.idx])
        if(res[0].affectedRows == 1) return {result: true, message: 'success'}
        else return {result: false, message: 'fail'}
    } catch(err) {
        throw Error(err)
    }
}

// preview, place 정보 모두제거
const removResources = async (idx) => {
    await pool.query('delete from PREVIEW where post_idx=?', [idx])
    await pool.query('delete from PLACE where post_idx=?', [idx])
    await image.delPrev(idx);
    await image.delMark(idx);
}

// kakao place api
const getKakaoPlace = async (keyword) => {      
    const options = {
        uri: 'https://dapi.kakao.com/v2/local/search/keyword.json',
        qs: {
            page: 1,
            size: 1,
            sort: 'accuracy',
            query: keyword
        },
        headers: {
            Authorization: 'KakaoAK '+env.KAKAO_REST_KEY
        },
        json: true
    };
    result = await request(options);
    
    if(result.documents.length>0) {
        let data = result.documents[0];
        return {
            title: data.place_name,
            description: data.category_name,
            address: data.road_address_name, 
            latitude: data.y, 
            longitude: data.x,
            url: data.place_url
        }
    }
    else return null;
}