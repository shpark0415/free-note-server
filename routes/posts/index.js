var express = require('express');
var router = express.Router();

const PostService = require('./post-service');
const customError = require('../error');

router.get('/categoryList', async (req, res) => {    
    const user_id = req.token.id

    try {
        let rows = await PostService.categoryList(user_id)
        if(rows) return res.send(rows);
        else return res.status(500).send(rows)
    } catch (err) {
        return customError(err, res);
    }
});

router.get('/addCategory', async (req, res) => {
    const user_id = req.token.id
    const params = req.query;

    try {
        let rows = await PostService.addCategory(user_id, params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.put('/modCategory', async (req, res) => {
    const user_id = req.token.id
    const params = req.body;

    try {
        let rows = await PostService.modCategory(user_id, params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.delete('/delCategory', async (req, res) => {
    const params = req.body;

    try {
        let rows = await PostService.delCategory(params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.get('/getMapInfo', async (req, res) => {
    const params = req.query;

    try {
        let rows = await PostService.getMapInfo(params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.post('/addPost', async (req, res) => {
    const user_id = req.token.id
    const params = req.body;

    try {
        let rows = await PostService.addPost(user_id, params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

// facebook
router.get('/getMarkFB', async (req, res) => {
    const params = req.query;
    
    try {
        let rows = await PostService.getMarkFB(params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.post('/addPostMarkFB', async (req, res) => {
    const user_id = req.token.id
    const params = req.body;

    try {
        let rows = await PostService.addPostMarkFB(user_id, params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.get('/getPost', async (req, res) => {
    const user_id = req.token.id
    const params = req.query;

    try {
        let rows = await PostService.getPost(user_id, params)
        if(rows === false) return res.status(404).json({result: false, message: 'not found'});
        else return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.post('/modPost', async (req, res) => {
    const user_id = req.token.id
    const params = req.body;

    try {
        let rows = await PostService.modPost(user_id, params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.post('/modPostMarkFB', async (req, res) => {
    const user_id = req.token.id
    const params = req.body;

    try {
        let rows = await PostService.modPostMarkFB(user_id, params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.delete('/delPost', async (req, res) => {
    const params = req.body;

    try {
        let rows = await PostService.delPost(params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

module.exports = router;
