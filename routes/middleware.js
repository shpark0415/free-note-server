const pool = require('../conf/db_info');
const jwt = require('jsonwebtoken');	   
const {SECRET_KEY} = require('../conf/token');

exports.chkAuth = async (req, res, next) => {
    const token = req.headers['authorization'].split("Bearer ")[1];

    if(token == 'null') {
        return res.status(401).json({  
            result: false,
            message: 'Unauthorized'
        });
    }    
    
    // 토큰으로 유효검사 후 next
    try {
        var decoded = await jwt.verify(token, SECRET_KEY);
        req.token = decoded;
        next();
    } catch(err) {
        return res.status(403).json({              
            result: false,
            message: "유효시간이 지났습니다. 다시 로그인해주세요."
        });
    }
}