const pool = require('../../conf/db_info');	   

exports.getMarks = async (user_id) => {
    const sql = 'select idx, post_idx, title, latitude, longitude from PLACE where user_id=?'

    try {
        let res = await pool.query(sql, user_id)
        res[0].map((item,idx)=>
            res[0][idx].title = item.title.replace(' : 네이버', '')
        )
        return res[0];
    } catch(err) {
        throw Error(err)
    }
}

exports.getDistrict = async (user_id, p) => {
    const kwArray = p.keyword.split(' ')
    let queryString = ''
    kwArray.map(k => {
        if(k !== '') queryString = queryString+'+'+k+'* ';
    })  // '+서울* +강남* '

    const sql = "select concat(시도명, ' ', 시군구명, ' ', 읍면동명) as district from ADDRESS "+
    "where match(시도명,시군구명,읍면동명) against(? IN BOOLEAN MODE) group by 시도명, 시군구명, 읍면동명;"

    try {
        let res = await pool.query(sql, [queryString])
        return res[0];
    } catch(err) {
        throw Error(err)
    }
}

exports.getCoordinates = async (user_id) => {
    const sql = "select latitude, longitude, zoom from USER_MAP where user_id=?"

    try {
        let res = await pool.query(sql, user_id)
        return res[0][0];
    } catch(err) {
        throw Error(err)
    }
}

exports.setCoordinates = async (user_id, p) => {
    const sql = "update USER_MAP set latitude=?, longitude=?, zoom=? where user_id=?"
    try {
        let res = await pool.query(sql, [p.latitude, p.longitude, p.zoom, user_id])
        if(res[0].affectedRows == 1) return {result: true, message: 'success'};
        else return {result: false, message: 'fail'};
    } catch(err) {
        throw Error(err)
    }
}