var express = require('express');
var router = express.Router();

const MapService = require('./map-service');
const customError = require('../error');

router.get('/getMarks', async (req, res) => {    
    const user_id = req.token.id

    try {
        let rows = await MapService.getMarks(user_id)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.get('/getDistrict', async (req, res) => {    
    const user_id = req.token.id
    let params = req.query;

    try {
        let rows = await MapService.getDistrict(user_id, params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.get('/getCoordinates', async (req, res) => {    
    const user_id = req.token.id

    try {
        let rows = await MapService.getCoordinates(user_id)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

router.put('/setCoordinates', async (req, res) => {    
    const user_id = req.token.id
    const params = req.body;

    try {
        let rows = await MapService.setCoordinates(user_id, params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

module.exports = router;
