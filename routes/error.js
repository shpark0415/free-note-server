const customError = (err, res) => {
    console.log(err)
    return res.status(500).json({
        result: false,
        message: err.message
    })
}

module.exports = customError