var express = require('express');
var router = express.Router();

const UserService = require('./user-service');
const middleware = require('../middleware.js')
const customError = require('../error');

router.post('/register', async (req, res) => {
    const params = req.body;

    try {
        let rows = await UserService.addUser(params)
        if(!rows) return res.json({
            result: false,
            message: '중복된 아이디가 존재합니다.'
        })
        else return res.json({
            result: true,
            message: '회원가입이 완료되었습니다!'
        })
    } catch (err) {
        return customError(err, res);
    }
});

router.post('/login', async (req, res) => {
    const params = req.body;
    
    try {
        let rows = await UserService.login(params)
        if(rows.result) return res.json(rows);
        else return res.status(500).json(rows)
    } catch (err) {
        return customError(err, res);
    }
});

router.post('/chkPwd', middleware.chkAuth, async (req, res) => {
    const params = req.body;
    
    try {
        let rows = await UserService.chkPwd(params)
        if(rows.result) return res.json(rows);
        else return res.status(500).json(rows)
    } catch (err) {
        return customError(err, res);
    }
});

router.get('/refresh', middleware.chkAuth, async (req, res) => {    
    try {
        return res.json({
            result: true,
            id: req.token.id
        });
    } catch (err) {
        return customError(err, res);
    }
});

module.exports = router;
