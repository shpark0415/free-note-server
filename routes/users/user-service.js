const pool = require('../../conf/db_info');	   
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {SECRET_KEY} = require('../../conf/token');

exports.addUser = async (params) => {
    let conn = await pool.getConnection()

    const chkId = 'select EXISTS(SELECT * FROM USER WHERE id=?) as isChk';
    const userSql = 'insert into USER set ?';
    const mapSql = 'insert into USER_MAP set user_id=?, '+
        'latitude=37.5627426613403, longitude=126.97849496534627, zoom=5';

    try {
        await conn.beginTransaction()

        let res1 = await pool.query(chkId, [params.id])   
        if(res1[0][0].isChk) return false;   // 중복아이디 존재

        const hash = await makeBcrypt(params.password);
        params.password = hash;

        let res2 = await pool.query(userSql, params)   
        let res3 = await pool.query(mapSql, [params.id])   

        await conn.commit()

        return true;        
    } catch(err) {
        throw Error(err)
    } finally {
        conn.release()
    }
}

exports.login = async (params) => {
    const chkId = 'SELECT * FROM USER WHERE id=?'

    try {
        let res1 = await pool.query(chkId, [params.id])   
        if(res1[0].length>0) {
            let res2 = await chkBcrypt(params.password, res1[0][0].password)
            if(res2) {
                const token = jwt.sign({ id: params.id }, SECRET_KEY, {expiresIn: params.maintain ? '100y' : '1h'});
                return {result: true, message: 'success', token: token}
            }
            else return {result: false, message: '비밀번호가 맞지 않습니다.'};
        }
        else return {result: false, message: '해당 아이디가 없습니다.'};
    } catch(err) {
        throw Error(err)
    }
}

exports.chkPwd = async (params) => {
    const chkId = 'SELECT * FROM USER WHERE id=?'

    try {
        let res1 = await pool.query(chkId, [params.id])   
        if(res1[0].length>0) {
            let res2 = await chkBcrypt(params.password, res1[0][0].password)
            if(res2) return {result: true, message: 'success'};
            else return {result: false, message: '비밀번호가 맞지 않습니다.'};
        }
        else return {result: false, message: '해당 아이디가 없습니다.'};
    } catch(err) {
        throw Error(err)
    }
}

const makeBcrypt = (password) => {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);
    return hash;
}

const chkBcrypt = (password, hash) => {
    return bcrypt.compareSync(password ,hash) ;
}