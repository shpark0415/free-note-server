const express = require('express');
const router = express.Router();

const middleware = require('./middleware.js')
const user = require('./users/index.js')
const post = require('./posts/index.js')
const list = require('./lists/index.js')
const map = require('./maps/index.js')

router.use('/user', user);
router.use('/post', middleware.chkAuth, post);
router.use('/list', middleware.chkAuth, list);
router.use('/map', middleware.chkAuth, map);

module.exports = router;