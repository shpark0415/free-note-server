var express = require('express');
var router = express.Router();

const ListService = require('./list-service');
const customError = require('../error');

router.get('/getAlarm', async (req, res) => {    
    const user_id = req.token.id

    try {
        let rows = await ListService.getAlarm(user_id)
        if(rows) return res.json(rows);
        else return res.status(500).json(rows)
    } catch (err) {
        return customError(err, res);
    }
});

router.post('/getList', async (req, res) => {    
    const user_id = req.token.id
    let params = req.body;

    try {
        let rows = await ListService.getList(user_id, params)
        if(rows) return res.json(rows);
        else return res.status(500).json(rows)
    } catch (err) {
        return customError(err, res);
    }
});

router.get('/getStarLists', async (req, res) => {    
    const user_id = req.token.id
    const params = req.query;

    try {
        let rows = await ListService.getStarLists(user_id, params)
        if(rows) return res.json(rows);
        else return res.status(500).json(rows)
    } catch (err) {
        return customError(err, res);
    }
});

router.get('/getExpireLists', async (req, res) => {    
    const user_id = req.token.id
    const params = req.query;

    try {
        let rows = await ListService.getExpireLists(user_id, params)
        if(rows) return res.json(rows);
        else return res.status(500).json(rows)
    } catch (err) {
        return customError(err, res);
    }
});

router.delete('/delExpireLists', async (req, res) => {    
    let params = req.body;

    try {
        let rows = await ListService.delExpireLists(params)
        return res.json(rows);
    } catch (err) {
        return customError(err, res);
    }
});

module.exports = router;
