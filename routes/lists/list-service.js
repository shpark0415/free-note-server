const pool = require('../../conf/db_info');	   
const env = require('../../conf/env');
const url = require('../../util/url');
const image = require('../../util/image');

exports.getAlarm = async (user_id) => {
    const sql = 'SELECT * FROM POST where user_id=?'

    try {
        let res = await pool.query(sql, [user_id])
        let alarm = []

        for(let i=0; i<res[0].length; i++) {
            // alarm 저장
            const dday = chkExpire(res[0][i].expire)
            if(dday !== -1 ){

                alarm.push({
                    id: res[0][i].idx,
                    title: await alarmTitle(res[0][i]),   // post에 없으면 preview 사용
                    description: await contentTitle(res[0][i]),
                    date: res[0][i].expire.slice(0,10),
                    dday: dday,
                    lock: res[0][i].lock
                })
            }
        }
        return alarm;
    } catch(err) {
        throw Error(err)
    }
}

exports.getList = async (user_id, p) => {
    let conn = await pool.getConnection()
    let sql;
    let allSql;
    let prevSql = 'SELECT * FROM PREVIEW where post_idx=?'

    try {
        await conn.beginTransaction()

        let res;
        let res2;

        if(p.category_idx === 0) {
            sql = 'select p.* from POST as p left join PREVIEW as r on p.idx = r.post_idx '+
                'where p.user_id=? and (p.title like '+pool.escape('%'+p.keyword+'%')+' or p.content like '+pool.escape('%'+p.keyword+'%')+' or r.title like '+pool.escape('%'+p.keyword+'%')+' or r.content like '+pool.escape('%'+p.keyword+'%')+') '+
                'order by p.idx desc limit ?,10'
            allSql = 'SELECT COUNT(*) AS allCnt from POST as p left join PREVIEW as r on p.idx = r.post_idx '+
            'WHERE user_id=? and (p.title like '+pool.escape('%'+p.keyword+'%')+' or p.content like '+pool.escape('%'+p.keyword+'%')+' or r.title like '+pool.escape('%'+p.keyword+'%')+' or r.content like '+pool.escape('%'+p.keyword+'%')+')'
            res = await pool.query(sql, [user_id, p.page*10])
            res2 = await pool.query(allSql, [user_id])
        } else {
            sql = 'select p.* from POST as p left join PREVIEW as r on p.idx = r.post_idx '+
                'where p.user_id=? and (p.title like '+pool.escape('%'+p.keyword+'%')+' or p.content like '+pool.escape('%'+p.keyword+'%')+' or r.title like '+pool.escape('%'+p.keyword+'%')+' or r.content like '+pool.escape('%'+p.keyword+'%')+') '+
                'and p.category_idx=? order by p.idx desc limit ?,10'
            allSql = 'SELECT COUNT(*) AS allCnt from POST as p left join PREVIEW as r on p.idx = r.post_idx '+
            'WHERE user_id=? and (p.title like '+pool.escape('%'+p.keyword+'%')+' or p.content like '+pool.escape('%'+p.keyword+'%')+' or r.title like '+pool.escape('%'+p.keyword+'%')+' or r.content like '+pool.escape('%'+p.keyword+'%')+') '+
            'AND category_idx=?'
            res = await pool.query(sql, [user_id, p.category_idx, p.page*10])
            res2 = await pool.query(allSql, [user_id, p.category_idx])
        }
        
        for(let i=0; i<res[0].length; i++) {
            // preview 저장
            let res3 = await pool.query(prevSql, [res[0][i].idx])
            res3[0][0] = url.getFilePath(res3[0][0])    // getFullPath
            res[0][i].preview = res3[0][0]
        }

        let chkEnd;
        if(p.category_idx === 0) chkEnd = await pool.query(sql, [user_id, (p.page+1)*10]);
        else chkEnd = await pool.query(sql, [user_id, p.category_idx, (p.page+1)*10]);

        let endPage = false;
        if(chkEnd[0].length === 0) endPage=true;
        await conn.commit()

        return {
            list: res[0],
            allCnt: res2[0][0].allCnt,
            endPage: endPage
        }
    } catch(err) {
        conn.rollback();
        throw Error(err)
    } finally {
        conn.release()
    }
}

exports.getStarLists = async (user_id, p) => {
    let conn = await pool.getConnection()
    const sql = 'SELECT * FROM POST where user_id=? and star>0 order by idx desc limit ?,10';
    const allSql = 'SELECT COUNT(*) AS allCnt FROM POST where user_id=? and star>0';
    const prevSql = 'SELECT * FROM PREVIEW where post_idx=?'

    try {
        await conn.beginTransaction()

        let res = await pool.query(sql, [user_id, p.page*10]) 
        let res2 = await pool.query(allSql, [user_id])

        for(let i=0; i<res[0].length; i++) {
            // preview 저장
            let res3 = await pool.query(prevSql, [res[0][i].idx])
            res3[0][0] = url.getFilePath(res3[0][0])    // getFullPath
            res[0][i].preview = res3[0][0]
        }

        let chkEnd = await pool.query(sql, [user_id, (p.page+1)*10]);

        let endPage = false;
        if(chkEnd[0].length === 0) endPage=true;
        await conn.commit()

        return {
            list: res[0],
            allCnt: res2[0][0].allCnt,
            endPage: endPage
        }
    } catch(err) {
        conn.rollback();
        throw Error(err)
    } finally {
        conn.release()
    }
}

exports.getExpireLists = async (user_id, p) => {
    let conn = await pool.getConnection()
    const sql = 'SELECT * FROM POST where user_id=? and expire-CURRENT_TIMESTAMP<0 order by idx desc limit ?,10;';
    const allSql = 'SELECT COUNT(*) AS allCnt FROM POST where user_id=? and expire-CURRENT_TIMESTAMP<0';
    const prevSql = 'SELECT * FROM PREVIEW where post_idx=?'

    try {
        await conn.beginTransaction()

        let res = await pool.query(sql, [user_id, p.page*10]) 
        let res2 = await pool.query(allSql, [user_id])

        for(let i=0; i<res[0].length; i++) {
            // preview 저장
            let res3 = await pool.query(prevSql, [res[0][i].idx])
            res3[0][0] = url.getFilePath(res3[0][0])    // getFullPath
            res[0][i].preview = res3[0][0]
        }

        let chkEnd = await pool.query(sql, [user_id, (p.page+1)*10]);

        let endPage = false;
        if(chkEnd[0].length === 0) endPage=true;
        await conn.commit()

        return {
            list: res[0],
            allCnt: res2[0][0].allCnt,
            endPage: endPage
        }
    } catch(err) {
        conn.rollback();
        throw Error(err)
    } finally {
        conn.release()
    }
}

exports.delExpireLists = async (params) => {
    let conn = await pool.getConnection()
    const sql = 'delete from POST where idx=?';
    
    try {
        await conn.beginTransaction()

        await params.list.map(async item=>{
            await removResources(item.idx)
            await pool.execute(sql, [item.idx])
        })

        await conn.commit()

        return {result: true, message: 'success'}
    } catch(err) {
        conn.rollback();
        throw Error(err)
    } finally {
        conn.release()
    }
}

const chkExpire = (expire) => {
    if(!expire) return -1;
    
    let dateDiff = Math.ceil((new Date(expire).getTime()-new Date().getTime())/(1000*3600*24));
    dateDiff = dateDiff - 1;
    if(dateDiff >= 0 && dateDiff <= 3) return dateDiff;
    else return -1;
}

const alarmTitle = async (item) => {
    if(item.title) return item.title;
    else {
        try {
            const prevSql = 'SELECT title FROM PREVIEW where post_idx=?'
            let res = await pool.query(prevSql, [item.idx])
            if(res[0].length >0 ) return res[0][0].title;
            else return '';
        } catch(err) {
            throw Error(err)
        }
    }
}

const contentTitle = async (item) => {
    if(item.content) return item.content;
    else {
        try {
            const prevSql = 'SELECT content FROM PREVIEW where post_idx=?'
            let res = await pool.query(prevSql, [item.idx])
            if(res[0].length >0 ) return res[0][0].content;
            else return '';
        } catch(err) {
            throw Error(err)
        }
    }
}

// preview, place 정보 모두제거
const removResources = async (idx) => {
    let conn = await pool.getConnection()
    try{
        await conn.beginTransaction()

        await pool.execute('delete from PREVIEW where post_idx=?', [idx])
        await pool.execute('delete from PLACE where post_idx=?', [idx])
        await conn.commit()
        await image.delPrev(idx);
        await image.delMark(idx);
    } catch(err) {
        conn.rollback();
        throw Error(err)
    } finally {
        conn.release()
    }
    
}