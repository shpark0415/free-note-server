const metascraper = require('metascraper')([
    require('metascraper-description')(),
    require('metascraper-image')(),
    require('metascraper-title')(),
    require('metascraper-url')()
])
  
const got = require('got');
const htmlEntityParse = require('html-entities');

exports.chkUrlType = async (url) => {
    try {
        if(url.indexOf('blog.naver.com') !== -1) return await naver(url);
        else if(url.indexOf('facebook.com') !== -1) return await facebook(url);
        else return await all(url);
    } catch (err) {
        throw Error(err)
    }
}

// naver blog
const naver = async (param) => {
    try {
        // 파라미터로 받은 url 주소
        let tempUrl = param.split('/');    
        const parseUrl = 'https://blog.naver.com/PostView.nhn?blogId='+tempUrl[3]+'&logNo='+tempUrl[4];

        const { body: html, url } = await got(parseUrl)
        let result = await metascraper({ html, url })
        if(result) {
            result.title = htmlEntityParse.decode(result.title)
            result.description = htmlEntityParse.decode(result.description);
        }

        console.log(result)
        return result;
    } catch(err) { 
        throw Error(err)
    }    
}

// facebook
const facebook = async (param) => {    
    try {
        // 파라미터로 받은 url 주소
        let parseUrl = param;
        if (parseUrl.indexOf('/?sfnsn=mo') !== -1) parseUrl = param.split('/?sfnsn=mo')[0];

        const { body: html, url } = await got(parseUrl, {
            headers: {
                // 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
                Cookie: "wd=1920x937; sb=vJ3ZX-RAoaMGzyCTcAcZv2pC; fr=1taeR5bl1QJRqCODH..Bf2Z24.a4.AAA.0.0.Bf2Z28.AWV7aKcbluM; datr=uJ3ZXxHA4u15uxM1oOJI4Bs2;"
            }
        })
        let result = await metascraper({ html, url })
        if (result) {
            result.title = htmlEntityParse.decode(result.title)
            result.description = htmlEntityParse.decode(result.description);            
            result.url = param;
        }
        console.log(result)

        return result;
    } catch(err) { 
        throw Error(err)
    }    
}

exports.facebookMark = async (param) => {    
    try {
        // 파라미터로 받은 url 주소
        let parseUrl = param;
        if (parseUrl.indexOf('/?sfnsn=mo') !== -1) parseUrl = param.split('/?sfnsn=mo')[0];

        const { body: html, url } = await got(parseUrl, {
            headers: {
                // 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
                Cookie: "wd=1920x937; sb=vJ3ZX-RAoaMGzyCTcAcZv2pC; fr=1taeR5bl1QJRqCODH..Bf2Z24.a4.AAA.0.0.Bf2Z28.AWV7aKcbluM; datr=uJ3ZXxHA4u15uxM1oOJI4Bs2;"
            }
        })
        let result = await metascraper({ html, url })
        if (result) {
            result.title = htmlEntityParse.decode(result.title)
            result.description = htmlEntityParse.decode(result.description);
            result.url = param;
        }
        console.log("test!!!", result)

        let getHiddenCode = html.match(/(<code id="u_0_1e">|<code id="u_0_1d">)(.*)(<\/code>)/g)
        const pattern = /(<span aria-label="&#xd574;&#xc2dc;&#xd0dc;&#xadf8;" class="_58cl _5afz">#<\/span><span class="_58cm">)(.*?)(<\/span>)/g;
        let keyword = []
        if (getHiddenCode) {
            let temp2 = getHiddenCode[0].match(pattern)
            temp2.map(item => {
                if (pattern.test(item)) {
                    keyword.push(item.replace(pattern, "$2"))
                }
            })
        }
        console.log("test!!!", keyword)
        
        return {
            preview: result,
	        tags: keyword
        }
    } catch(err) { 
        throw Error(err)
    }    
}

// all
const all = async (param) => {
    try {
        const { body: html, url } = await got(param)
        let result = await metascraper({ html, url })
        if(result) {
            result.title = htmlEntityParse.decode(result.title)
            result.description = htmlEntityParse.decode(result.description);
        }
        console.log(result)
        return result;
    } catch(err) { 
        throw Error(err)
    }
}