const request = require('request-promise');
const cheerio = require('cheerio');
const got = require('got');

exports.chkUrlType = async (url) => {
    try {
        if(url.indexOf('blog.naver.com') !== -1) return await nBlog(url);
        else if(url.indexOf('post.naver.com') !== -1) return await nPost(url);
        else return [];
    } catch (err) {
        throw Error(err)
    }
}

// naver blog
const nBlog = async (url) => {
    let result = [] // 최종 보내는 데이터

    try {
        // 파라미터로 받은 url 주소
        const tempUrl = url.split('/');    
        const parseUrl = 'https://blog.naver.com/PostView.nhn?blogId='+tempUrl[3]+'&logNo='+tempUrl[4];

        const html = await request(parseUrl);
        const $ = cheerio.load ( html,
            { decodeEntities: false }   // 한글 변환
        );

        const elements = $(".se-module-map-text").find("a");
        if(elements) {
            elements.map(item=> {
                let parseData = JSON.parse(elements[item].attribs['data-linkdata'])
                result.push({
                    title: parseData.name,
                    address: parseData.address,
                    url: 'https://m.place.naver.com/restaurant/'+parseData.placeId,
                    latitude: parseFloat(parseData.latitude),
                    longitude: parseFloat(parseData.longitude),
                })
            })
        }
        
        console.log(result);
        return result;
    } catch(err) { 
        throw Error(err)
    }    
}

// naver post
const nPost = async (url) => {
    let result = [] // 최종 보내는 데이터
    
    try {
        const { body: html } = await got(url)
        let temp = html.match(/(class="__se_module_data" data-module="{&quot;data&quot;:).*?}/g)
        if(temp) {
            temp.map(item=> {
                let parse = item.split(`class="__se_module_data" data-module="{&quot;data&quot;:`)[1]
                let parse2 = JSON.parse(parse.replace(/(&quot;)/g, `"`))
                result.push({
                    title: parse2.title,
                    address: parse2.address,
                    url: 'https://m.place.naver.com/restaurant/'+parse2.locationId,
                    latitude: parseFloat(parse2.latitude),
                    longitude: parseFloat(parse2.longitude),
                })
            })
        }
        
        console.log(result);
        return result;
    } catch(err) { 
        throw Error(err)
    }    
}