const DEVELOP = 'http://localhost:3005';                // 로컬 개발 서버
const PRODUCTION = 'http://jinmdev.cafe24.com:3005';    // 운영 서버

function generator() {
	return process.env.NODE_ENV === 'DEVELOPMENT' ? DEVELOP : PRODUCTION;
}

exports.BASEURL = generator();
exports.PRODUCTION = PRODUCTION;
exports.KAKAO_REST_KEY = '83be5cce881f5853c0475646144370a8';